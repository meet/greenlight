# meet.coop greenlight

[![pipeline status](https://git.coop/meet/greenlight/badges/v2/pipeline.svg)](https://git.coop/meet/greenlight/-/commits/v2)

## Outline of approach

This is a modified [upstream v2 branch](https://github.com/bigbluebutton/greenlight/tree/v2) which includes some of our own custom commits. In order to avoid confusion over Git branches and such, we just use one, the `v2` branch. We then merge upstream changes into this branch and manually resolve any conflicts from our own custom commits.

## Staying up-to-date

```bash
$ git remote add upstream https://github.com/bigbluebutton/greenlight.git
$ git pull upstream v2
$ git push origin v2
```
